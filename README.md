# BashTools

This git repo provides a set of extended bash tools, e.g. grep, find, and ls functions.  

BashTools will add a set of additional aliases divided in 7 categories: 1) Terminal, 2) Directory, 3) Locate, 4) Edition, 5) Grep & Search, 6) Find, and 7) Miscellaneous.

## Installation

The following installation instructions have been tested in Ubuntu 20.04

### Dependencies

Install the following ubuntu packages:

```bash
sudo apt install build-essential tree
```

### Global installation

The Bashtools uses CMake to simplify the install and uninstall process. Also for simplicity, we will install these Bashtools globally.

#### 1. Install the Bashtools scripts

```bash
git clone git@gitlab.com:volvo_gto/gpss_mvp/shared/tools/bashtools.git
cd bashtools/
sudo ./install_bin.sh
```

#### 2. Add the new bash extras to the .bash_aliases

NOTE: If you are creating the .bash_aliases file for the first time, make sure that the file begins with **#!/bin/bash** header

Open the .bash_aliases file

```bash
gedit ~/.bash_aliases
```

and copy the following text:

```bash
#!/bin/bash

########################BashTools###############################
    # Define the preferred text editor, e.g. gvim, gedit, kate, etc.
    pEditor="gedit"
    
    #INSTALL BashTools First
    if [ -f /usr/bin/bashextras.sh ]; then
    	source /usr/bin/bashextras.sh
    fi
    
    #Custom echo colors
    if [ -f ~/.colors.sh ]; then
    	source ~/.colors.sh
    else
    	source /usr/bin/bashcolors.sh
    fi
    
    #### CUSTOM SOURCES ###############
    # Custom aliases
    if [ -f ~/.bash_aliases.local ]; then
    	source ~/.bash_aliases.local 
    fi
```

#### 3 Re-source the environment

```bash
source ~/.bashrc
```

### Getting information

To list all the additional aliases and scripts provided by BashTools you can run in a terminal the following command:

```bash
opts
```

You should see a menu with the different options this package provides:

```bash
################ List of Additional Aliases ################			

Terminal aliases (xopt)                                                                                                       
+  h:                   Show command history 
+  pu: <dir>:           pushd <dir>
+  po:                  popd 
+  follow <filePath>:   Change to the dir containing the target file, and saves the current dir in the stack.
+  c|clc:               clear
+  r:                   reset
+  e|x:                 exit
+  rwx <filePath>:      Shows the access rights in octal format: rwx file.txt --> 775 file.txt
+  sized <dir>:         Shows the size of a dir and its subdirs.
+  mkdircd <dirPath>:   Creates a dir (it can use subdirs) and moves to the newly created dir.
+  cdu <dir>:           cd into target dir. If there is more than one dir with the same name, 
                        then it shows the available options and waits for user input.
                        The current dir is added to the push stack. You can use 'po' to return to the previous dir.
+  sshagent <-d>        Starts the ssh-agent. It requests the passphrase for the ssh key. If the option -d is used then the ssh-agent is removed.

LS and Dir aliases (lsopt)                                                                                                     
+  lls:            list all
+  lh:             list all except hidden files
+  lsa:            list hidden files
+  lsc:            list all files and dirs in a single column and no attributes
+  lsf:            list only files
+  lsfs:           list files and symbolic links
+  lsfh:           list files and hidden files
+  lsfhs:          list all except dirs
+  lsd:            list only dirs
+  lsds:           list dirs and symbolic links to a dirs
+  lss:            list only symbolic links
+  lssn:           list only symbolic links names
+  treel <levels>: Shows the first n-levels of the current dir tree. Levels must be an integer.
+  treetime:       Shows dir tree with date and time  (includes permisions, user, group and size)
+  treetimes:      Shows dir tree with date and time
+  lsg:            List all available groups in the system.
+  All the LS aliases accept the 'c/r' option which clears/resets the terminal and run the desired command, for example:
+  llsc = clear && lls
+  llsr = reset && lls

Locate and Update aliases (luopt)                                                                                            
+  upenv:	       Update environment loads .bashrc, .bash_aliases and rosSetup.bash
+  lo-:		       Locate  | less

Edition aliases (edopt)                                                                                                     
+  remspecials <string>:       Removes special characters from a string and changes it to CammelCase format.
                               i.e. (' |/'-->'-'), (',|;|:|}'-->'') 
+  2upA4 <file.ps>:	       Changes file.ps  to  A4 format file_2up.ps
+  2upletter <file.ps>:        Changes file.ps  to  letter format file_2up.ps
+  pdf2png [file_with_path]:   Converts multiple or single pdf files to png. Use '-h' option for more details.
+  eds <file>:                 Opens a file , using the user defined editor in .bash_aliases (gvim).
+  whiched:  	               Shows the user defined editor as defined in .bash_aliases (gvim).
+  editx <file_name>:          Touches file_name, adding 'x' rights, and opens it with eds (editor defined in .bash_alias).
+  gvimt <file>:               Opens a file in gvim as tab in the same window.
+  gvimm <file 1> ... [file 2]:Opens a file or files in multiple tabs (gvim).
+  (g)vimdiffr <local_file_path> <user@remote_srv> <remote_filepath>:
                               Shows the differences between local file local_file_path 
                               and a remote file remote_file_path in the server remote_srv
+  swapfd <target1> <target2>: Swaps target1 with target2, where target is either a file or a dir.
+  pdfresize <target1> <res>:  Resizes a pdf file target1 the target resolution res.
                               Type pdfresize --help to see the different options.
+  webm2avi target1.web: Converts target1.web to avi format.
```

Optional, each category has its own menu, e.g. Find --> findopt, Edition --> edopt, etc. See the output of 'opts' command for more details.

### Uninstall

To uninstall the scripts just run in the BashTools path

```bash
cd /path/to/BashTools
./uninstall.sh
```
