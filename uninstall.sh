#!/bin/bash

currentDir="$(pwd)"
echo ${currentDir}

if [ ! -d build-global ]; then
	echo "ERROR: no 'build-global dir"	
	exit 1
fi

cd build-global
sudo make uninstall
cd $currentDir
rm -rf build-global

