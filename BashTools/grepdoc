#!/bin/bash
# Chalmers
# Emmanuel Dean deanee@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
scriptsDir=$(
  cd $(dirname $sourceDir)
  pwd
)

scriptName="grepdoc"

if [[ $sourceDir != $runDir ]]; then
  echoc BRED "ERROR: You must run this script."
  return 1
fi

help() {
  echoc BWHITE NAME
  echoc "        grepdoc"
  echoc BWHITE SYNOPSIS
  echoc BWHITE "	grepdoc " BLUE "<target_string> "
  echoc BWHITE DESCRIPTION
  echoc BWHITE "	grepdoc " NOCOLOR "finds (grep) recursively the string " BLUE "target_string " NOCOLOR "in all " YELLOW "doc" NOCOLOR ", " YELLOW "docx" NOCOLOR " documents."
  echoc BWHITE AUTHORS
  echoc WHITE "        Emmanuel Dean <deane@chalmers.se>"
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  help
  exit 1
fi

if [ $# -eq 0 ]; then
  echoc BLRED "ERROR: No arguments supplied. See: " BWHITE "${scriptName} --help"
  help
  exit 1
fi

echoc WHITE "Target string: " BYELLOW "$@"

find . -name "*.doc" |
  while read i; do catdoc "$i" |
    grep --color=auto -iH --label="$i" "$@"; done
find . -name "*.docx" |
  while read i; do docx2txt <"$i" |
    grep --color=auto -iH --label="$i" "$@"; done
