#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se


# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
scriptsDir=$(cd $(dirname $sourceDir); pwd)

if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi

#The out put of the function is the first echo!!!
function replaceSpecial()
{
    local aux=""
    for (( i=0; i<${#1}; i++ )); do
        #echoc  BRED "Letter ${1:$i:1}"
        
        if [[ ${1:$i:1} == *"/"* ]]; then
            #echoc  BGREEN "Replace Letter ${1:$i:1}"
            aux=$aux"\\"${1:$i:1}
        else
            aux=$aux${1:$i:1}   
        fi
        #echoc  BCYAN "Aux ${aux}"
    done
    echo "$aux"
}

findHelp()
{
	echoc BWHITE NAME
	echoc "        findrepl"
	echoc BWHITE SYNOPSIS
	echoc BWHITE "	findrepl " YELLOW "[options] " BLUE "<target_string> " GREEN "<new_string> " CYAN "[file]"
	echoc BWHITE DESCRIPTION
	echoc BWHITE "	findrepl " NOCOLOR "finds a string " BLUE "target_string " NOCOLOR "and replace it with the string " GREEN "new_string " NOCOLOR "in all the files in the " UWHITE "current" NOCOLOR " dir." 
   echoc NOCOLOR "	If " CYAN "file " NOCOLOR "is provided, then it will replace the string only in that target file."
   echoc BWHITE "	findrepl " NOCOLOR "has as two " YELLOW "options" NOCOLOR ": " YELLOW "-r " NOCOLOR "and " YELLOW "-v."
   echoc NOCOLOR "        If " YELLOW "-r " NOCOLOR "is used, then the search of the " BLUE "target_string " NOCOLOR "is done recursively. This option has no effect if " CYAN "file " NOCOLOR "is provided."
   echoc NOCOLOR "        If " YELLOW "-v " NOCOLOR "is used, then the script will only " UWHITE "list" NOCOLOR " the target files founded (dryrun mode)."
   echoc NOCOLOR "        The " YELLOW "options " NOCOLOR "can be combined, e.g. " YELLOW "-rv " NOCOLOR "or " YELLOW "-vr" NOCOLOR "."
	echoc BWHITE AUTHORS
	echoc WHITE "        Emmanuel Dean <deane@chalmers.se>"
	echoc WHITE "        Florian Bergner <florian.bergner@chalmers.se>"
}

if [ $# -eq 0 ] || [ $# -gt 4 ] 
  then
	 echoc BLRED "ERROR: Wrong number of arguments:"
	 findHelp
	 exit 1
fi

#Array of input arguments
inargs=( "$@" )

#analizing the first argument to search for options
##first char of input arg 1
optsymb="${1:0:1}"

#Index where the targets start from the input arguments
indx=0
dryrun="false"
recurflag="false"
if [ $optsymb == "-" ]; then   
   if [[ $1 == *"h"* ]]; then
      findHelp
      exit 0
   fi
   if [[ $1 == *"r"* ]]; then
      recurflag="true"
   fi
   if [[ $1 == *"v"* ]]; then
      dryrun="true"
      echoc BALYELLOW "Dry run mode, the targets won't be changed!"
   fi
   indx=1
fi

if [ $recurflag == "true" ] || [ $dryrun == "true" ]; then
   if [ $# -lt 3 ]; then
      echoc BLRED "ERROR: Wrong number of arguments:"
      findHelp
      exit 1
   fi
fi

#Target string
targetstr=( ${inargs[${indx}]} )

#echoc GREEN "Target string: " BGREEN "$targetstr"
# Search for / character and replace it with \/
targetstr=$(replaceSpecial "$targetstr")
#echoc  BWHITE "Subtituted ${targetstr}"



#Replacement string
rindx=$[ $indx + 1 ]
replstr=( ${inargs[${rindx}]} )
#echoc GREEN "Replace string: " BGREEN "$replstr"
replstr=$(replaceSpecial "$replstr")
#echoc  BGRAY "Subtituted ${replstr}"

#target file (the replacement will only take place in this file)
findx=$[ $indx + 2 ]

# The number of arguments is less than than findx, i.e. no target file provided
if [ $# -le $findx ]; then
   
   if [ $recurflag == "true" ]; then
      files=$(grep -rli "$targetstr" *)
   else
      files=$(grep -li "$targetstr" *)
   fi
   
   
   if [[ $files = *[!\ ]* ]];
      then
         echoc BWHITE "Replacing " BLYELLOW "$targetstr " BWHITE "with " BLGREEN "$replstr " BWHITE "in files: "
         echoc BLBLUE "$files" 
      if [ $dryrun != "true" ]; then
         grep -rli "$targetstr" * | xargs -i@ sed -i "s/${targetstr}/${replstr}/g" @
#       else
#          echoc BALYELLOW "Dry run mode, the targets won't be changed!"
      fi
   else
         echoc BRED "The string " BLYELLOW "$targetstr " BRED "couldn't be found in any file!!!" 
   fi 
   
else
   
   #Target file (changes will only made in this file)
   targetf=( ${inargs[${findx}]} )

   files=$(grep -li "$targetstr" $targetf)


   files=$(grep -rli "$targetstr" "$targetf")
   if [[ $files = *[!\ ]* ]];
      then
         echoc BWHITE "Replacing " BLYELLOW "$targetstr " BWHITE "with " BLGREEN "$replstr " BWHITE "in file " BBLUE "$targetf " 
         if [ $dryrun != "true" ]; then
            grep -rli "$targetstr" "$targetf" | xargs -i@ sed -i "s/${targetstr}/${replstr}/g" @
#          else
#             echoc BALYELLOW "Dry run mode, the targets won't be changed!"
         fi

   else
         echoc BRED "The string " BLYELLOW "$targetstr " BRED "couldn't be found in the file: " BBLUE "${targetf} " BRED "!!!" 
   fi 
fi

exit 1
