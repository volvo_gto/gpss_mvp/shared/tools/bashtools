#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0

bashExtrasDir=$(cd $(dirname $sourceDir); pwd)

if [[ $sourceDir == $runDir ]]; then
	echoc BRED "ERROR: You must source this script."
	exit 1
fi

#################################
######## ADDITIONAL ALIASES #####
#################################


xopt()
{
	echoc BALBLUE "\nTerminal aliases (xopt)                                                                                                       " 
	echoc BALBLUE "+ " LYELLOW " h:" WHITE "                   Show command history " 
	echoc BALBLUE "+ " LYELLOW " pu:" LCYAN " <dir>" LYELLOW ":" WHITE "           pushd <dir>" 
	echoc BALBLUE "+ " LYELLOW " po:" WHITE "                  popd " 
	echoc BALBLUE "+ " LYELLOW " follow" LCYAN " <filePath>" LYELLOW ":" WHITE "   Change to the dir containing the target file, and saves the current dir in the stack."   
	echoc BALBLUE "+ " LYELLOW " c" LGREEN "|" LYELLOW "clc:" WHITE "               clear" 	
	echoc BALBLUE "+ " LYELLOW " r:" WHITE "                   reset" 
	echoc BALBLUE "+ " LYELLOW " e" LGREEN "|" LYELLOW "x:" WHITE "                 exit"
	echoc BALBLUE "+ " LYELLOW " rwx " LCYAN "<filePath>" LYELLOW ":" WHITE "      Shows the access rights in octal format: " LGREEN "rwx file.txt " WHITE "--> " LRED "775 file.txt" 
	echoc BALBLUE "+ " LYELLOW " sized " LCYAN "<dir>" LYELLOW ":" WHITE "         Shows the size of a dir and its subdirs." 
	echoc BALBLUE "+ " LYELLOW " mkdircd " LCYAN "<dirPath>" LYELLOW ":" WHITE "   Creates a dir " LGREEN "(it can use subdirs) " WHITE "and moves to the newly created dir." 
	echoc BALBLUE "+ " LYELLOW " cdu " LCYAN "<dir>" LYELLOW ":" WHITE "           cd into target " LGREEN "dir" WHITE ". If there's more than one dir with the same name, "
    echoc WHITE   "                        then it shows the available options and waits for user input." 
    echoc WHITE   "                        The current dir is added to the push stack. You can use 'po' to return to the previous dir."
	echoc BALBLUE "+ " LYELLOW " sshagent " LCYAN "<-d> " WHITE "       Starts the ssh-agent. It requests the passphrase for the ssh key. If the option " LGREEN "-d " WHITE "is used then the ssh-agent is removed."   

    # Show local menu (defined in .bash_aliases.local)
    if [ "$xOpt" ]
    then
        xoptl
    fi

    
}

alias xoptc='clear && xopt'
alias xoptr='reset && xopt' 

lsopt()
{
	echoc BALRED "\nLS and Dir aliases (lsopt)                                                                                                     " 
	echoc BALRED "+ " LYELLOW " lls:           " WHITE " list " LGREEN "all" 
	echoc BALRED "+ " LYELLOW " lh:            " WHITE " list " LGREEN "all " LRED "except hidden files" 
	echoc BALRED "+ " LYELLOW " lsa:           " WHITE " list " LGREEN "hidden " LRED "files" 
	echoc BALRED "+ " LYELLOW " lsc:           " WHITE " list " LGREEN "all " WHITE "files and dirs in a " LRED "single column and no attributes" 
	echoc BALRED "+ " LYELLOW " lsf:           " WHITE " list " LGREEN "only " LRED "files" 
	echoc BALRED "+ " LYELLOW " lsfs:          " WHITE " list " LRED "files " WHITE "and " LRED "symbolic links" 
	echoc BALRED "+ " LYELLOW " lsfh:          " WHITE " list " LRED "files " WHITE "and " LRED "hidden files" 
	echoc BALRED "+ " LYELLOW " lsfhs:         " WHITE " list " LGREEN "all " LRED "except dirs" 
	echoc BALRED "+ " LYELLOW " lsd:           " WHITE " list " LGREEN "only " LRED "dirs" 
	echoc BALRED "+ " LYELLOW " lsds:          " WHITE " list " LRED "dirs and symbolic links to a dirs" 
	echoc BALRED "+ " LYELLOW " lss:           " WHITE " list " LGREEN "only " LRED "symbolic links" 
	echoc BALRED "+ " LYELLOW " lssn:          " WHITE " list " LGREEN "only " LRED "symbolic links names" 
    echoc BALRED "+ " LYELLOW " treel " LCYAN "<levels>" LYELLOW ":" WHITE " Shows the first n-" LRED "levels" WHITE " of the current dir tree. "  LRED "Levels " WHITE "must be an integer."        
    echoc BALRED "+ " LYELLOW " treetime:     " WHITE "  Shows dir " LGREEN "tree " WHITE "with " LRED "date " WHITE "and " LRED "time " WHITE " (includes permisions, user, group and size)" 
    echoc BALRED "+ " LYELLOW " treetimes:    " WHITE "  Shows dir " LGREEN "tree " WHITE "with " LRED "date " WHITE "and " LRED "time" 
    echoc BALRED "+ " LYELLOW " lsg:" WHITE "            List all available " LGREEN "groups" WHITE " in the system."
	echoc BALRED "+ " WHITE " All the LS aliases accept the " LGREEN "'c/r' " WHITE "option which " BYELLOW "clears/resets " BWHITE "the terminal and " BYELLOW "run " BWHITE "the desired " BYELLOW "command" BWHITE ", for example:" 
   echoc BALRED "+ " LYELLOW " lls" BLRED "c" WHITE " = " LRED "clear " LGREEN "&& lls" 
   echoc BALRED "+ " LYELLOW " lls" BLGREEN "r" WHITE " = " LRED "reset " LGREEN "&& lls" 
}
alias lsoptc='clear && lsopt'
alias lsoptr='reset && lsopt'

luopt()
{
	echoc BALGREEN "\nLocate and Update aliases (luopt)                                                                                            " 
	echoc BALGREEN "+ " LYELLOW " upenv:	      " WHITE " Update environment loads " LGREEN ".bashrc" WHITE ", " LGREEN ".bash_aliases" WHITE " and " LGREEN "rosSetup.bash" 
	echoc BALGREEN "+ " LYELLOW " lo-:		    " LRED "   Locate " LGREEN " | less" 

    # Show local menu (defined in .bash_aliases.local)
    if [ "$luOpt" ]
    then
        luoptl
    fi
}
alias luoptc='clear && luopt'
alias luoptr='reset && luopt'

edopt()
{
	echoc BALYELLOW "\nEdition aliases (edopt)                                                                                                     " 
	echoc BALYELLOW "+ " LYELLOW " remspecials " LCYAN "<string>" LYELLOW ":" WHITE "       Removes special characters from a string and changes it to CammelCase format." 
    
    echoc BALYELLOW "  " WHITE "                             i.e. (" LGREEN "' |/'" WHITE "-->" LRED "'-'" WHITE "), (" LGREEN "',|;|:|}'" WHITE "-->" LRED "''" WHITE ") " 
	echoc BALYELLOW "+ " LYELLOW " 2upA4 " LCYAN "<file.ps>" LYELLOW ":" WHITE "	       Changes " LGREEN "file.ps " WHITE " to " LGREEN " A4 format " LRED "file_2up.ps"
	echoc BALYELLOW "+ " LYELLOW " 2upletter " LCYAN "<file.ps>" LYELLOW ":" WHITE "        Changes " LGREEN "file.ps " WHITE " to " LGREEN " letter format " LRED "file_2up.ps"
    echoc BALYELLOW "+ " LYELLOW " pdf2png " LCYAN "[file_with_path]" LYELLOW ":" WHITE "   Converts " LGREEN "multiple " WHITE "or " LRED "single " WHITE "pdf files to png. " LYELLOW "Use " LCYAN "'-h'" LYELLOW " option for more details."
    echoc BALYELLOW "+ " LYELLOW " eds " LCYAN "<file>" LYELLOW ":" WHITE "                 Opens a " LCYAN "file " WHITE ", using the user defined editor in .bash_aliases (" LGREEN "gvim" WHITE ")."
    echoc BALYELLOW "+ " LYELLOW " whiched:" WHITE "  	               Shows the user defined " LCYAN "editor" WHITE " as defined in .bash_aliases (" LGREEN "gvim" WHITE ")."
    echoc BALYELLOW "+ " LYELLOW " editx " LCYAN "<file_name>" LYELLOW ":" WHITE "          Touches " LGREEN "file_name" WHITE ", adding " LRED "'x' " WHITE "rights, and " LYELLOW "opens " WHITE "it with eds (editor defined in .bash_alias)."
    echoc BALYELLOW "+ " LYELLOW " gvimt " LCYAN "<file>" LYELLOW ":" WHITE "               Opens a " LCYAN "file " WHITE "in gvim as tab in the same window." 
    echoc BALYELLOW "+ " LYELLOW " gvimm " LCYAN "<file 1> ... [file 2]" LYELLOW ":" WHITE "Opens a " LCYAN "file " WHITE "or " LBLUE "files" WHITE " in multiple tabs (" LGREEN "gvim" WHITE ")." 
    echoc BALYELLOW "+ " LYELLOW " (g)vimdiffr " LCYAN "<local_file_path> <user@remote_srv> <remote_filepath>" LYELLOW ":"
    echoc BALYELLOW "  " WHITE "                             Shows the differences between local file " LYELLOW "local_file_path "
    echoc BALYELLOW "  " WHITE "                             and a remote file " LGREEN "remote_file_path" WHITE " in the server " LBLUE "remote_srv" 
    echoc BALYELLOW "+ " LYELLOW " swapfd " LCYAN "<target1> <target2>" LYELLOW ":" WHITE " Swaps " LCYAN "target1 " WHITE "with " LGREEN "target2" WHITE ", where " LBLUE "target" WHITE " is either a file or a dir."
    echoc BALYELLOW "+ " LYELLOW " pdfresize " LCYAN "<target1> <res>" LYELLOW ":" WHITE "  Resizes a pdf file " LCYAN "target1 " WHITE "the target resolution " LGREEN "res" WHITE "."
    echoc BALYELLOW "  " WHITE "                             Type " BWHITE "pdfresize --help" WHITE " to see the different options." 
    echoc BALYELLOW "+ " LYELLOW " webm2avi " LCYAN "target1.web" LYELLOW ":" WHITE " Converts " LCYAN "target1.web " WHITE "to " LGREEN "avi" WHITE " format."

    # Show local menu (defined in .bash_aliases.local)
    if [ "$edOpt" ]
    then
        edoptl
    fi
}
alias edoptc='clear && edopt'
alias edoptr='reset && edopt'

grepopt()
{
	echoc BALPURPLE "\nGrep & Search aliases (grepopt)                                                                                             "
	echoc BALPURPLE "+ " LYELLOW " grepenv " LCYAN "<target>" LYELLOW ":" WHITE " 	        Search for " LGREEN "'target' " WHITE "in the " LRED "environment variables" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " grepenvw " LCYAN "<target>" LYELLOW ":" WHITE " 	        Search for " LGREEN "'target' " WHITE "in the " LRED "environment variables" WHITE ". (" LYELLOW "exact match" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " grepr " LCYAN "<target>" LYELLOW ":" WHITE "	        Search for " LGREEN "'target' " LRED "recursively."
	echoc BALPURPLE "+ " LYELLOW " grepr- " LCYAN "<target>" LYELLOW ":" WHITE "	        Search for " LGREEN "'target' " LRED "recursively" WHITE " with " LGREEN "| less " WHITE " option."
	echoc BALPURPLE "+ " LYELLOW " greprf " LCYAN "<target>" LYELLOW ":" WHITE "	        Search for " LGREEN "'target' " LRED "recursively. " LYELLOW " Only shows the name of the file."
	echoc BALPURPLE "+ " LYELLOW " greprw " LCYAN "<target>" LYELLOW ":" WHITE "	        Search the for " LGREEN "'target' " LRED "recursively " WHITE "(" LYELLOW "exact match" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " greprwf " LCYAN "<target>" LYELLOW ":" WHITE "	        Search the for " LGREEN "'target' " LRED "recursively " WHITE "(" LYELLOW "exact match and only shows the name of the file" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " grepxclude " LCYAN "<target> <dir>" LYELLOW ":" WHITE "   Search for " LGREEN "'target' " LRED "recursively" WHITE ", excluding folder " LRED "'dir'" WHITE "."
    echoc BALPURPLE "+ " LYELLOW " greppdf " LCYAN "<target> [pdf_file]" LYELLOW ":" WHITE " Search for " LGREEN "'target' " WHITE "either in " LRED "pdf_file" WHITE " (if it's provided), or find " URED "recursively" WHITE " all " LRED "*.pdf" WHITE " and " 
    echoc BALPURPLE "  " WHITE "                              search inside the files."
    echoc BALPURPLE "+ " LYELLOW " grepdoc " LCYAN "<target>" LYELLOW ":" WHITE "            Search for " LGREEN "'target' " WHITE "string recursively in all " LRED "*.doc/*.docx" WHITE " files."	
    echoc BALPURPLE "+ " LYELLOW " grepps " LCYAN "<process_name>" LYELLOW ":" WHITE "       Search for " LGREEN "'process_name' " WHITE "in the " LRED "running processes" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " greppsw " LCYAN "<process_name>" LYELLOW ":" WHITE "      Search for " LGREEN "'process_name' " WHITE "in the " LRED "running processes" WHITE ". (" LYELLOW "exact match" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " grepg: " LCYAN "<target>" LYELLOW ": " WHITE "            Search for " LGREEN "'target'" WHITE " in the " LRED "system groups" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " grepgw: " LCYAN "<target>" LYELLOW ": " WHITE "           Search for " LGREEN "'target'" WHITE " in the " LRED "system groups" WHITE ". (" LYELLOW "exact match" WHITE ")."
    echoc BALPURPLE "+ " LYELLOW " grepugw: " LCYAN "<user>" LYELLOW ": " WHITE "            Search for a " LGREEN "'user'" WHITE " in the " LRED "system groups" WHITE ". (" LYELLOW "exact match" WHITE ")." 
    echoc BALPURPLE "+ " LYELLOW " grepug: " LCYAN "<user>" LYELLOW ": " WHITE "             Search for a " LGREEN "'user'" WHITE " in the " LRED "system groups" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " grepdpkg: " LCYAN "<target>" LYELLOW ": " WHITE "         Search for " LGREEN "'target'" WHITE " in the " LRED "installed debian package list" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " grepdpkgw: " LCYAN "<target>" LYELLOW ": " WHITE "        Search for " LGREEN "'target'" WHITE " in the " LRED "installed debian package list" WHITE ". (" LYELLOW "exact match" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " greph: " LCYAN "<target>" LYELLOW ": " WHITE "            Search for " LGREEN "'target'" WHITE " in the " LRED "history" WHITE "."
	echoc BALPURPLE "+ " LYELLOW " grephw: " LCYAN "<target>" LYELLOW ": " WHITE "           Search for " LGREEN "'target'" WHITE " in the " LRED "history" WHITE ". (" LYELLOW "exact match" WHITE ")."
	echoc BALPURPLE "+ " LYELLOW " grepo: " LCYAN "<target>" LYELLOW ": " WHITE "            Search for " LGREEN "'target'" WHITE " in " LRED "opts" WHITE " alias menu."
	echoc BALPURPLE "+ " LYELLOW " grepow: " LCYAN "<target>" LYELLOW ": " WHITE "           Search for " LGREEN "'target'" WHITE " in " LRED "opts" WHITE " alias menu. (" LYELLOW "exact match" WHITE ")."

}
alias grepoptc='clear && grepopt'
alias grepoptr='reset && grepopt'

miscopt()
{
	echoc BALCYAN "\nMiscellaneous aliases (miscopt)                                                                                               " 
	echoc BALCYAN "+ " LYELLOW " octavenogui:" WHITE " 	     Run octave without GUI."
	echoc BALCYAN "+ " LYELLOW " dtime"  LCYAN " [options]" LYELLOW ": " WHITE "         prints the uptime in the same format as dmesg. "
    echoc WHITE "                             If the " LCYAN "option " BWHITE "-a "  WHITE "is used, then both the " BWHITE "uptime " WHITE "and " BWHITE "idle " WHITE "time are printed."  
	echoc BALCYAN "+ " LYELLOW " showcolors: " WHITE "              Shows the available " LGREEN "colors" WHITE " and their corresponding " LBLUE "labels" WHITE ". To be used with " BWHITE "echoc" WHITE "."

    # Show local menu (defined in .bash_aliases.local)
    if [ "$miscOpt" ]
    then
        miscoptl
    fi
}
alias miscoptc='clear && miscopt'
alias miscoptr='reset && miscopt'


findopt()
{
    echoc BALGRAY "\nFind aliases (findopt)                                                                                                         " 
	echoc BALGRAY "+ " LYELLOW " findf" LCYAN " <target>" LYELLOW ": " WHITE "          Finds " LYELLOW "files" WHITE " matching " LGREEN "target " WHITE "recursively."
	echoc BALGRAY "+ " LYELLOW " findrexf" LCYAN " <target>" LYELLOW ": " WHITE "       Finds " LYELLOW "files" WHITE " matching " LGREEN "target " WHITE "(using" LRED " regexp" WHITE ".)"
	echoc BALGRAY "+ " LYELLOW " findd" LCYAN " <target>" LYELLOW ": " WHITE "          Finds " LYELLOW "dirs" WHITE " matching " LGREEN "target " WHITE "recursively."
	echoc BALGRAY "+ " LYELLOW " findrexd" LCYAN " <target>" LYELLOW ": " WHITE "       Finds " LYELLOW "dirs" WHITE " matching " LGREEN "target " WHITE "(using" LRED " regexp" WHITE ".)"
	echoc BALGRAY "+ " LYELLOW " findfx" LCYAN " <target> <dir>" LYELLOW ": " WHITE "   Finds " LYELLOW "files" WHITE " matching " LGREEN "target " WHITE "recursively, excluding " LGREEN "dir" WHITE "." 
    echoc BALGRAY "+ " LYELLOW " findrexfx" LCYAN " <target> <dir>" LYELLOW ": " WHITE "Finds " LYELLOW "files" WHITE " matching " LGREEN "target " WHITE "(using " LRED "regex" WHITE ") excluding " LGREEN "dir" WHITE "in the search." 
	echoc BALGRAY "+ " LYELLOW " finddx" LCYAN " <target> <dir>" LYELLOW ": " WHITE "   Finds " LYELLOW "dirs" WHITE " matching " LGREEN "target " WHITE "recursively, excluding " LGREEN "dir" WHITE "." 
    echoc BALGRAY "+ " LYELLOW " findrexdx" LCYAN " <target> <dir>" LYELLOW ": " WHITE "Finds " LYELLOW "dirs" WHITE " matching " LGREEN "target " WHITE "(using " LRED "regex" WHITE ") excluding " LGREEN "dir" WHITE "in the search." 
	echoc BALGRAY "+ " LYELLOW " findrepl" LCYAN " [opts] <target> <new> [file]" LYELLOW ":"
    echoc BALGRAY "  " WHITE "    	                     Finds  a string " LGREEN "target" WHITE " in a " LBLUE "file " WHITE "and replaces it with " LGREEN "new" WHITE "." 
    echoc BALGRAY "  " WHITE "                           Type " BWHITE "findrepl --help" WHITE " to see the different options." 
	echoc BALGRAY "+ " LYELLOW " findrm" LCYAN " [opts] <target> [dir1] [dirn]" LYELLOW ":"
    echoc BALGRAY "  " WHITE "    	                     Finds  a file/dir " LGREEN "target" WHITE " and deletes it ." 
    echoc BALGRAY "  " WHITE "                           Type " BWHITE "findrm --help" WHITE " to see the different options." 
	echoc BALGRAY "+ " LYELLOW " findtemp:" WHITE "    	     Finds " LGREEN "'*~'" WHITE " (backup) files " LRED "recursively."
	echoc BALGRAY "+ " LYELLOW " findrmtemp:" WHITE "  	     Finds and " LYELLOW "Removes " LGREEN "'*~'" WHITE " (backup) files " LRED "recursively."
	echoc BALGRAY "+ " LYELLOW " findemptydir:" WHITE "             Finds all " BRED "emtpy" WHITE " directories."
	echoc BALGRAY "+ " LYELLOW " findemptydirx" LCYAN " <dir>" LYELLOW ": " WHITE "     Finds all " BRED "emtpy" WHITE " directories, excluding " LGREEN "dir" WHITE "."
    echoc BALGRAY "+ " LYELLOW " findregex " LCYAN "<dir> [-type <type>] -name <target>" LYELLOW ": " WHITE "Finds " LYELLOW "files" WHITE " or|and a " LYELLOW "dirs" WHITE " depending on " LBLUE "type" WHITE ", matching " LGREEN "target " WHITE "(using " LRED "regexp" WHITE ".)"
}
alias findoptc='clear && findopt'
alias findoptr='reset && findopt'

#Show alias menu for all the sections
opts()
{
	echoc BAWHITE "################ List of Additional Aliases ################			"
	
	#Terminal
	xopt	
	#LS
	lsopt
	#Locate and Update
	luopt
	#Edition 
	edopt
	#Grep and Search
	grepopt
	#Find 
    findopt
	#Miscellaneous
	miscopt
}
alias optsc='clear && opts'
alias optsr='reset && opts'
alias bashtoolsinfo='opts'

#### TERMINAL ALIASES ################################################b
alias h='history'
alias j="jobs -l"
alias pu="pushd"
alias po="popd"
alias c="clear"
alias r="reset"
alias clc="clear"
alias e="exit"
alias x="exit"
#get the access rights in octal: accrights file.txt --> 775 file.txt
alias rwx="stat -c '%a - %n'"
#Get the size of a folder recursivelly 
sized()
{
	echo $1
	du -hsc $1/*

}

#Create a Folder and cd into it
mkdircd()
{
	mkdir -p $1
   cd $1
}

#Look for dir and cd into it
cdu()
{
    array=()
    while IFS=  read -r -d $'\0'; do
        array+=("$REPLY")
    done < <(find . -name ${@} -not -path "*.git/*" -print0)

    #echoc RED "${#array[@]}"
    #printf "%s\n" "${array[@]}"
    #echo "${array[@]}"
    
    if [ ${#array[@]} -eq 0 ];
    then
        echoc RED "Dir: " ULRED "$@/" RED " not found"
        return 1
    fi 

    if [ ${#array[@]} -gt 1 ];
    then
        echoc GREEN "More than one option, please select: "
        for ((i = 0 ; i < ${#array[@]}; i++ ));
        do 
           echoc YELLOW "$((i+1)): " LBLUE "${array[$i]}"
        done

        read opt

        opt2=$((opt-1))
        #echo "$opt2"

        echoc LYELLOW "Jumping into dir [" YELLOW "$opt" LYELLOW "]: " GREEN "${array[$opt2]}/"
        pu "${array[$opt2]}"

        #for i in "${array[@]}"
        #do 
        #   echoc YELLOW "$i:" 
        #done
    else
        echoc LYELLOW "Jumping into dir: " GREEN "${array[0]}/"
        pu "${array[0]}"
    fi  
}

sshagent()
{
    if [ "$#" -eq 0 ];
        then
        #echoc "exec ssh-agent bash"
        echoc BYELLOW "Starting ssh-agent"
        eval `ssh-agent`
        #echoc "ssh-add"
        ssh-add
    fi

    if [ "$1" == "-d" ];
    then
        echoc BYELLOW "Removing ssh-agent"
        ssh-add -D
    fi
}


#Follow a file
followHelp()
{
	echoc BWHITE NAME
	echoc "        follow"
	echoc BWHITE SYNOPSIS
	echoc NOCOLOR "	follow <" YELLOW "file" NOCOLOR ">"
	echoc BWHITE DESCRIPTION
	echoc NOCOLOR "	follow changes to the directory containing the target " YELLOW "file" NOCOLOR ", and adds the current dir to the directory stack, then you can retrieve the last directory using popd."
	echoc BWHITE AUTHORS
	echoc WHITE "        Emmanuel Dean <deane@chalmers.se>"
	echoc WHITE "        Florian Bergner <florian.bergner@chalmers.se>"
}

follow()
{
   if [ "$1" == "-h" ] || [ "$1" == "--help" ];
   then
      followHelp
      return 0
   fi

   if [ $# -eq 0 ] || [ $# -gt 1 ] 
   then
      echoc BLRED "ERROR: Wrong number of arguments. See: " BWHITE "follow --help"
      followHelp
      return 1
   fi

   tdir=$(dirname "$1")

   pushd "$tdir" >/dev/null 2>&1

   echoc YELLOW "Moved to:" BYELLOW "$PWD"
}




#### TERMINAL ALIASES ################################################e


#### LS ALIASES####################################################b

#Show all 
alias lls='ls -halsF'

#Show all except hidden files
alias lh='ls -lh'

#Show all files and dirs as a column
alias lsc='ls -1'

#Show only hidden files
alias lsa='ls -ld .?*'

#Show only files
lsf()
{
	ls --color=always -1 -F $@ | grep -v / | grep -v @
}


# List all available groups in the system
alias lsg='cut -d: -f1 /etc/group'

# Shows the first level branches of a directory tree 
treel()
{
    tree -L $1 
}

# Shows directory tree with date and time
treetime()
{
    tree --timefmt %D-%r -tpugh "$@"
}

treetimes()
{
    # -t orders the files chronologically
    tree --timefmt %D-%r -t "$@"
}

#Show only symbolic links names
lssn()
{
	ls --color=always -1 -F $@ | grep -i @ | cut -f1 -d'@'
}
#Show only symbolic links
lss()
{
	ls --color=always -ltra $@ | grep '^l'
}

#Show files and symbolic links
lsfs()
{
	ls --color=always -1 -F $@ | grep -v / | cut -f1 -d'@'
}

#Show files and hiden files
lsfh()
{
	ls --color=always -A1 -F $@ | grep -v / | grep -v @
}

#Show files, hiden files and symbolic links
lsfhs()
{
	ls --color=always -A1 -F $@ | grep -v / | cut -f1 -d'@'
}

#Show only dirs
lsd()
{
	ls --color=always  -1 -F $@ | grep -i /
}

#Show dirs and symbolic links dirs
lsds()
{
	ls --color=always  -1 -F $@ | grep -i /
	if [ $# -eq 0 ]; then
		find -P . -maxdepth 1 -type l -xtype d -exec echo -e -n "${BLCYAN}{} -> ${BLBLUE}" \; -exec readlink {} \; 
		return 0
	fi
   find -P $@ -maxdepth 1 -type l -xtype d -exec echo -e -n "${BLCYAN}{} -> ${BLBLUE}" \; -exec readlink {} \;
}

#Show all 
alias llsc="clear && lls"
#Show all except hidden files
alias lhc="clear && lh"
alias lscc="clear && lsc"
#Show only files
alias lsfc="clear && lsf"
#Show only symbolic links
alias lssnc="clear && lssn"
alias lssc="clear && lss"
#Show files and symbolic links
alias lsfsc="clear && lsfs"
#Show files and hiden files
alias lsfhc="clear && lsfh"
#Show files, hiden files and symbolic links
alias lsfhsc="clear && lsfhs"
#Show only dirs
alias lsdc="clear && lsd"
#Show dirs and symbolic linkcs to a dirs
alias lsdsc="clear && lsds"

#Show list of groups 
alias lsgc="clear && lsg"


#Show all 
alias llsr="reset && lls"
#Show all except hidden files
alias lhr="reset && clear && lh"
alias lscr="reset && clear && lsc"
#Show only files
alias lsfr="reset && clear && lsf"
#Show only symbolic links
alias lssnr="reset && clear && lssn"
alias lssr="reset && clear && lss"
#Show files and symbolic links
alias lsfsr="reset && clear && lsfs"
#Show files and hiden files
alias lsfhr="reset && clear && lsfh"
#Show files, hiden files and symbolic links
alias lsfhsr="reset && clear && lsfhs"
#Show only dirs
alias lsdr="reset && clear && lsd"
#Show dirs and symbolic linkcs to a dirs
alias lsdsr="reset && lsds"

#Show list of groups 
alias lsgr="reset && lsg"
#### LS ALIASES####################################################e

#### LOCATE and UPDATEDB #####################################################################b
alias upenv='source ~/.bashrc'


lo-()
{
   locate $1 | less
}

#### LOCATE and UPDATE #####################################################################e


#### EDITION #####################################################################################################
eds()
{
    "$pEditor" $1 &
}

alias whiched='echoc YELLOW "Current editor: " LYELLOW "$pEditor"' 

#Removes special characters (e.g. '-,}/') from a string, and transforms the string into CammelCase
remspecials()
{
    #\u capitalize first letter
    #\U capitalize all letters
	echo "$@" | sed "s/  */-/g" | sed "s/, *//g" | sed "s/; *//g" | sed "s/: *//g" | sed "s/\/ */-/g" | sed "s/} *//g" | sed "s/'s/s/g" | sed -e "s/[(|)]//g" | sed -r "s/\w+/\u&/g" | sed "s/-//g" 
} 

2upA4()
{
   A=`basename $1 .ps`
   pstops '2:0L@.7(21.59cm,0)+1L@.7(21.59cm,13.97cm)' $A".ps" $A"_2up.ps"
 }


2upletter ()
{
  A=`basename $1 .ps`
  pstops '2:0L@.7(21.59cm,0)+1L@.7(21.59cm,13.97cm)' $A".ps" $A"_2up.ps"
}

printPDF2PNGHelp()
{
	echo "Help -- How to use this alias"
	echo "The script can be used in the following form:"
	echo "1) ///Find all *.pdf files in the current dir and convert them to png"
	echo "$ pdf2png"
	echo "2) ///Convert <File_with_path> to png"
	echo "$ pdf2png File_with_path"
}

pdf2png()
{
	case "$#" in
	"0")
		find . -type f -name "*.pdf"|while read line
		do
			#echo "line:" $line
			dir=${line%/*}
			#echo "dir:" $dir
			file=${line##*/}
			#echo "file:" $file
			file=${file%.*}
			#echo "file:" $file
			#convert -transparent white -fuzz 10% $line ${file}.png
			convert $line ${file}.png
   			echo "converting" ${file}.pdf to ${file}.png
			#echo "////////////////"
		done
		;;
	"1")
		if [ "$1" == "-h" ];
		then
			printPDF2PNGHelp
			return 1
		else		
			file=${1##*/}
			#echo "file:" $file
			file=${file%.*}
			#echo "file:" $file
			convert $1 ${file}.png
		fi		
		;;
	esac

}
pdfresize()
{	

	fname=$(basename $1 .pdf)
	
	pdflevel=$2

	fname_in=$fname".pdf"
	fname_out=$fname"_s.pdf"
	fname_out_nb=$fname"_snb.pdf"

	

	echo -e "$LYELLOW Inputname: 		  "$fname_in"\e[0m"
	echo -e "$LGREEN Outputname: 		  "$fname_out"\e[0m"
	echo -e "$LRED Outputname no bookmarks: "$fname_out_nb"\e[0m"

	gs -sDEVICE=pdfwrite -dCompatibilityLevel=$pdflevel -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=$fname_out $fname_in

	#Remove bookmarks
	#pdftk $fname_out cat output $fname_out_nb 
}

webm2avi()
{
    file_name=${1%%.*}
    output="$file_name.avi"
    echoc YELLOW "Converting $1 to " BYELLOW "$output"
    ffmpeg -i $1 -qscale 0 $output

}

#open file as a tab in an already oppened session of gvim
alias gvimt="gvim --remote-tab-silent"

#Open multiple files in tabs
gvimm()
{
    # if there's only one argument or none just open it
    gvim --remote-tab-silent $1 &
    
    #wait a bit to let the first window to load
    sleep 2
    
    if [ $# -gt 1 ]
    then
        for i in "${@:2}"
        do
            echoc YELLOW "Tab: " LYELLOW "$i"
            gvim --remote-tab-silent "$i"
        done
    fi
}

# creates a file with x flag and opens editor
editx()
{
	touch $1
	chmod +x $1
	eds $1
}

#diff with a remote file
gvimdiffr()
{
    if [ $# -lt 3 ] 
        then
        echoc BLRED "ERROR: Wrong number of arguments use: " WHITE "gvimdiffr " BBLUE "local_filepath" BYELLOW " usr@srv " BGREEN "remote_filepath"
    	return 1
    fi

    gvimdiff "$1" "scp://$2//$3" 

}

vimdiffr()
{
    if [ $# -lt 3 ] 
        then
        echoc BLRED "ERROR: Wrong number of arguments use: " WHITE "vimdiffr " BBLUE "local_filepath" BYELLOW " usr@srv " BGREEN "remote_filepath"
    	return 1
    fi

    vimdiff "$1" "scp://$2//$3" 

}



swapfd()
{
    if [ $# != 2 ] 
    then
        echoc BLRED "ERROR: Wrong number of arguments use: " WHITE "swapfd " BBLUE "file1" BYELLOW " file2"
    	return 1
    fi


    mv "$1" "$1.tempo"
    mv "$2" "$1"
    mv "$1.tempo" "$2"
}
#### EDITION #####################################################################################################e

#### GREP & SEARCH ########################################################################b
#grep alias menu (opts)
grepo()
{
    opts | grep --color -i $1
}

grepow()
{
    opts | grep --color -w $1
}

#grep history
greph()
{
    history | grep --color -i $1
}

#grep history exact match
grephw()
{
    history | grep --color -w $1
}

#grep list of ubuntu packages
grepdpkg()
{
    dpkg -l | grep --color -i $1
}

grepdpkgw()
{
    dpkg -l | grep --color -iw $1
}

#grep groups
grepg()
{
    lsg | grep --color -i $1
}

grepgw()
{
    lsg | grep --color -iw $1
}

#grep user in groups
grepug()
{
    cat /etc/group | grep --color -i $1
}

grepugw()
{
    cat /etc/group | grep --color -iw $1
}

#grep the environment variables
grepenv()
{
	env | grep --color -i $1
}

grepenvw()
{
	env | grep --color -iw $1
}

#grep inside pdf files
greppdf()
{
        
    if [ $# -eq 0 ] || [ $# -gt 2 ] 
        then
        echoc BLRED "ERROR: Wrong number of arguments use: " WHITE "greppdf " BBLUE "<target_string>" BYELLOW " [pdf_file]"
    	return 1
    fi
    
    if [ $# -eq 1 ] 
    then
    	#echoc YELLOW "Searching for: " LYELLOW "$1" YELLOW " in all PDF files." 
        #find . -name '*.pdf' -exec echo {} \; -exec pdftotext {} - \; | grep -in "$1"
        while IFS=  read -r -d $'\0'; do
                #op=$(pdftotext "${REPLY}" - | grep -in ${1})
                op=$(pdftotext "${REPLY}" - )
                if [[ $op == *"${1}"* ]]; then
                    echoc YELLOW "Found " BYELLOW "${1}" YELLOW " in " BGREEN "$REPLY"
                    echo "$op" | grep --color -i "$1"
                fi
        done < <(find . -name '*.pdf' -print0)
    return 0
    fi
    
    if [ $# -eq 2 ] 
    then
        op=$(pdftotext "${2}" - )

        if [[ $op == *"${1}"* ]]; then
            echoc YELLOW "Found " BYELLOW "${1}" YELLOW " in " BGREEN "$2"
            echo "$op" | grep --color -i "$1"
        fi
        return 0 
    fi
}

#grep process list
grepps()
{
    # The last grep --color $1 is to show the matching strings in color
	ps ax | grep $1 | grep -v "grep" |  grep --color $1
}


#Recursive grep eXcluding dir
grepxclude()
{
	grep -R --color --exclude-dir=$2 -n $1 *
}

#Recursive grep eXact match
greprw()
{
	grep --color -rwn $1 *
}

#Recursive grep eXact match only file names
greprwf()
{
	grep --color -rlw $1 *
}

#Recursive grep less
grepr- ()
{
  	grep --color -rn $1 * | less
}

#Recursive grep 
grepr ()
{
  	grep --color -rn $1 *
}

#Recursive grep only file names
greprf ()
{
  	grep --color -lr $1 *
}

#### GREP ########################################################################e

#### MISCELLANEOUS ################################################################
alias octavenogui='octave --no-gui'

#shows uptime
dtime()
{
    ut=($(cat /proc/uptime))
    
    echoc BYELLOW "UpTime: " YELLOW "${ut[0]}"     

    if [ $# -gt 0 ] 
        then
	    echoc BGREEN  "IdleTime: " GREEN "${ut[1]}"    
	    return -1
    fi
}

#### MISCELLANEOUS ################################################################

#### FIND ################################################################
alias findrmtemp='find . -iname "*~" -type f | xargs -I% rm -f %'
alias findtemp='find . -iname "*~" -type f '
alias findemptydir='find . -type d -empty'

# find using regex (experimental)
findregx()
{
    # The standard use of this function is:
    # findregex <path> -name <target> -type <type>
    
    
    
    if [ $# -eq 0 ]
    then
       echoc BLRED "ERROR: Wrong number of arguments. Use: " WHITE "findregex <path> -name <target> -type <type>"
    
        return 1
    fi
    
    targetIdx=""
    typeIdx=""
    notPath=""
    array=()
    count=0
    for i in $@;
    do
        array+=($i)
        
        #echoc CYAN "${array[$count]}"
    
    
        #echoc GREEN "$count"
        #echoc BYELLOW "$i";
        if [[ $i == "-type" ]]
        then
            typeIdx=$((count+1))
        fi
        
        if [[ $i == "-name" ]]
        then
            targetIdx=$((count+1))
        fi

        if [[ $i == "-path" ]]
        then
            notPath=$((count+1))
        fi

        
        count=$((count+1))
        #echoc GREEN "count=$count typeIdx=$typeIdx targetIdx=$targetIdx"
    
    
    done
    
    # echo ${array[0]}
    # echo ${array[$typeIdx]}
    # echo ${array[$targetIdx]}
    

    nPath=${array[$notPath]%/*} 

    # A nested if is required to handle the -not -path, but I'm not sure if is needed.
    # Use the other findrex options
    echo ${nPath} 
    echo ${array[$notPath]} 
    if [[ $typeIdx == "" ]]
    then
        find "${array[0]}" -regextype posix-egrep -regex "${array[$targetIdx]}" 
    else
        find "${array[0]}" -type ${array[$typeIdx]} -regextype posix-egrep -regex "${array[$targetIdx]}"

    fi

      return 0

}

# find a file using regex
findrexf()
{
    find . -type f -regextype posix-egrep -regex  "$@"
}

# find a dir using regex
findrexd()
{
    find . -type d -regextype posix-egrep -regex  "$@"
}

# find a file using regex and excluding a dir 
findrexfx()
{
    find . -type f -regextype posix-egrep -regex  "$1" -not -path "*${2%/*}/*"
}

# find a dir using regex and excluding a dir 
findrexdx()
{
    find . -type d -regextype posix-egrep -regex  "$1" -not -path "*${2%/*}/*"
}


# find an empty dir excluding a dir 
findemptydirx()
{
   find . -type d -not -path "$@/*" -empty
   #For multiple dirs
#    find . -type d -not -path "./.git/modules/*"  -not -path "./.git/refs/*" -empty
}

#std find file
findf()
{
    find . -type f -name "$@"
}

#std find dir
findd()
{
    find . -type d -name "$@"
}

#std find file, excluding dir
findfx()
{
    find . -type f -name "$1" -not -path "*${2%/*}/*"
}

#std find dir, excluding dir
finddx()
{
    find . -type d -name "$1" -not -path "*${2%/*}/*"
}

#### FIND ################################################################


#### NETWORK #############################################################

arplocalnames()
{
    # Scan the local network and retrieves only the ip addresses
    a=$(sudo arp-scan -l | grep -oP '^[\d.][\d.]+')

    # Get the name of the host with a given ip address
    for i in $a
    do
        host $i
    done
}




