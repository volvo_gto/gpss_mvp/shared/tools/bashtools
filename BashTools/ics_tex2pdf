#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
scriptsDir=$(cd $(dirname $sourceDir); pwd)
scriptName="ics_tex2pdf"
self=$BASH_SOURCE


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi

once=false
cleanup()
{
    if [ "$once" = false ] && [ "$usesTmpDir" = true ] ; then
        if [ -z "$tmpDir" ]; then
            once=true
            return
        fi
    
        # delete temporary dir
        echo "Delete temporary directory '$tmpDir'..."
        rm -R ${tmpDir}
        
        once=true
    fi
}

# catch Ctrl+C
trap "cleanup" 2
trap "cleanup" EXIT


fileExt()
{   
    namePath=$1

    {
        name=$(basename $namePath)
    } 2>/dev/null
    
    echo ${name##*.}    
}

fileName()
{   
    namePath=$1
    
    {
        name=$(basename $namePath)
    } 2>/dev/null
    
    echo ${name%.*}   
}

filePath()
{   
    namePath=$1
    {
        echo $(dirname $namePath)
    } 2>/dev/null
}

checkFileExt()
{
    namePath=$1
    ext=$2
    
    if [[ "$(fileExt $namePath)" != "$ext" ]]; then
        return 1
    fi
    
    return 0
}


help()
{
    echoc BWHITE NAME
    echoc "        ${scriptName}"
    echoc BWHITE SYNOPSIS
    echoc NOCOLOR "	${scriptName} " YELLOW "<file.tex>" LBLUE " [output.pdf]"
    echoc NOCOLOR "	${scriptName} " YELLOW "<file1.tex> <file2.tex> ..."
    echoc NOCOLOR "	${scriptName} " GREEN "-d " YELLOW "<file.tex> " LBLUE "<tmpDir>"
    echoc BWHITE DESCRIPTION
    echoc BWHITE "	${scriptName}" NOCOLOR " creates a cropped .pdf file of " YELLOW "file.tex" WHITE "."
    echoc NOCOLOR "	The default output name is " LYELLOW "file.pdf" NOCOLOR "."
    echoc NOCOLOR "	If " LBLUE "output.pdf" NOCOLOR " is provided, then the output file will be named accordingly."
    echoc NOCOLOR "	If " LGREEN "-d" NOCOLOR " is provided, then " BWHITE "${scriptName}" NOCOLOR " will work with the temporary folder " LBLUE "tmpDir" NOCOLOR "."
    echoc NOCOLOR "	All intermediate files and the output file " LYELLOW "file.pdf" NOCOLOR " will stay in that folder."
    echoc BWHITE AUTHORS
    echoc WHITE "        Florian Bergner <florian.bergner@chalmers.se>"
    echoc WHITE "        Emmanuel Dean <deane@chalmers.se>"
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
    help
    exit 1
fi

if [ $# -eq 0 ]
then
    echoc BLRED "ERROR: No arguments supplied. See: " BWHITE "${scriptName} --help"
    help
    exit 1
fi


# default settings
inExt=tex
outExt=pdf  

usesTmpDir=true
multiIn=false

inList=()
inFileNamePath=
outFileNamePath=
tmpDir=

ERR_OK=0
ERR_NUM_ARGS=1
ERR_IN_FILE_EXT=2
ERR_OUT_FILE_EXT=3
ERR_ARG=10

parseErr()
{
    if [ $1 -eq $ERR_OK ]; then
        echo "No Error."
    fi
    
    if [ $1 -eq $ERR_NUM_ARGS ]; then
        echo "Invalid number of input arguments."
    fi
    
    if [ $1 -eq $ERR_IN_FILE_EXT ]; then
        echo "Invalid input file extension."
    fi
    
    if [ $1 -eq $ERR_OUT_FILE_EXT ]; then
        echo "Invalid output file extension."
    fi
    
    if [ $1 -eq $ERR_ARG ]; then
        echo "Invalid input arguments."
    fi
}

parseMultiArgs()
{
    #multiIn=true
    args=($@)
    nargs=${#args[@]}

    for (( i=0; i<nargs; i++ )); do
        arg=${args[${i}]}

        if ! checkFileExt $arg $inExt; then
            return $ERR_ARG
        fi
        
        inList+=($arg)
    done
    
    if [ ${#inList[@]} -lt 1 ]; then
        return $ERR_ARG
    fi
    
    multiIn=true
    return $ERR_OK
}


parseArgs()
{
    # special option -d
    if [ $# -eq 3 ] && [ "$1" == "-d" ]; then
        if ! checkFileExt $2 $inExt; then
            return $ERR_IN_FILE_EXT
        fi
        
        usesTmpDir=false
        inFileNamePath=$2
        tmpDir=$3
        return $ERR_OK
    fi

    if [ $# -lt 3 ] && [ "$1" == "-d" ]; then
        return $ERR_NUM_ARGS
    fi


    # one arg
    if [ $# -eq 1 ]; then
        if ! checkFileExt $1 $inExt; then
            return $ERR_IN_FILE_EXT
        fi
    
        inFileNamePath=$1
        return $ERR_OK
    fi

    # two args
    if [ $# -eq 2 ]; then
        if ! checkFileExt $1 $inExt; then
            return $ERR_IN_FILE_EXT
        fi

        if checkFileExt $2 $inExt; then
            parseMultiArgs $@
            return $?
        fi

        if ! checkFileExt $2 $outExt; then
            return $ERR_OUT_FILE_EXT
        fi
        
        inFileNamePath=$1    
        outFileNamePath=$2 
        return $ERR_OK
    fi
    
    # three args or more
    if [ $# -ge 3 ]; then
        parseMultiArgs $@
        return $?
    fi
    
    return $ERR_ARG
}

parseArgs $@
ret=$?

if [ ! $ret -eq 0 ] ; then
    echoc BLRED "ERROR: $(parseErr $ret) See: " BWHITE "${scriptName} --help"
    help
    exit 1
fi

# handle multi with recursion
if [ "$multiIn" = true ] ; then
    echo "Multiple input args..."
    
    for arg in ${inList[@]}; do
        inFileNamePath=$arg
        inFileName=$(fileName $inFileNamePath)
        inFileExt=$(fileExt $inFileNamePath)
        inFilePath=$(filePath $inFileNamePath)

        logFile=$inFileName.log
        {
            $self $arg
        } > $logFile 2> $logFile
        
        if [[ $? == 0 ]]; then
            rm $logFile
        fi
    done
        
    exit
fi


# create default output if necessary
if [ -z $outFileNamePath ]; then
    name=$(fileName $inFileNamePath)   
    outFileNamePath=$(pwd)/$name.$outExt
fi

# overwrite default output if tmpDir is provided
if [ ! -z $tmpDir ]; then
    name=$(fileName $inFileNamePath)   
    outFileNamePath=$tmpDir/$name.$outExt
fi

# create temp dir if necessary
if [ "$usesTmpDir" = true ] ; then
    # create tempory directory
    tmpDir=$(mktemp -d)
fi


# create support variables for input/output
inFileName=$(fileName $inFileNamePath)
inFileExt=$(fileExt $inFileNamePath)
inFilePath=$(filePath $inFileNamePath)

outFileName=$(fileName $outFileNamePath)
outFileExt=$(fileExt $outFileNamePath)
outFilePath=$(filePath $outFileNamePath)


# check input and output settings
if [ ! -f $inFileNamePath ]; then
    echoc BLRED "ERROR: The input file '${inFileNamePath}' doesn't exist."
    exit 1
fi

if [ ! -d $outFilePath ]; then
    echoc BLRED "ERROR: The output path '${outFilePath}' doesn't exist."
    exit 1
fi


echo "Create the .pdf file..."
pdflatex -halt-on-error -output-directory=$tmpDir $inFileNamePath

if [[ $? != 0 ]]; then
    exit 1
fi

echo "Crop the .pdf file..."
pdfcrop $tmpDir/$inFileName.pdf $tmpDir/${inFileName}_c.pdf

if [[ $? != 0 ]]; then
    exit 1
fi

# rename/copy file
cp -v $tmpDir/${inFileName}_c.pdf $outFileNamePath


